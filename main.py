import logging, time, random, sys
from telethon.sync import TelegramClient
from telethon import events, functions, types, errors
from telethon.tl.functions.messages import ImportChatInviteRequest
from telethon.tl.functions.channels import JoinChannelRequest, LeaveChannelRequest



class texts:
    ping = "✅ربات فعال است✅"
    pray = [
    "یا علی(ع)",
    "یا حسن(ع)",
    "یا حسین(ع)",
    "یا فاطمه(س)",
    "یا زهرا(س)",
    "یا علی مدد",
    "یا رضا(ع)"
    ]
    
    invalid_err = "لینک گروه منقضی شده و یا اینکه برای شما قابل دسترس نیست❗️"
    invalid_link = "لینک اشتباه است❗️"
    error = """
متاسفانه خطایی حین چک کردن لینک رخ داد❌
خطای رخ داده:
{}




    """
    entit_error = """
    متاسفانه خطایی رخ داد❌
    خطای رخ داده:
    {}
    
    

    
    """
    expected_error = """
    متاسفانه خطایی رخ داد❌
    {}
    """
    full_join = "شما عضو کانال ها و گروه های زیادی شده اید و به محدودیت عضویت رسیده اید❗️"
 
    wrong_type = """
ما نوع ریپورتی به نام {} نداریم❗️❗️❗️
ریپورت های موجود :
```Spam```
```Violence```
```Porn```
```ChildAbuse```
```GeoIrrelevant```
```Scam```

    """
    join = """
    درحال عضو شدن داخل لینک ...
    """
    check = """
    در حال بررسی لینک ...
    """
    start = "در حال شروع عملیات...🔰"
    checking = "در حال چک کردن..."
    link_type_err = """
    خطایی رخ داد❗️
    ما نوع لینک {} را نداریم
    
    نوع لینک ها:
```Public```
```Private```
    """
    result_form = """
عملیات به اتمام رسید

زمان صرف شده⏱:{}

لینک گروه🌐:
{}
نوع ریپورت ها🚫:
{}

نتیجه عملیات:

تعداد ریپورت های موفق✅: {}
تعداد ریپورت های ناموفق❕: {}

**{}**

خطاها❗️: {}





"""
    msg_result_form = """
عملیات به اتمام رسید

زمان صرف شده⏱:{}

لینک گروه🌐:
{}
نوع ریپورت ها🚫:
{}

شناسه عددی پیام ها💡:{}

نتیجه عملیات:

تعداد ریپورت های موفق✅: {}
تعداد ریپورت های ناموفق❕: {}

**{}**

خطاها❗️: {}




    
    """
    now_result = """
درحال انجام درخواست...
تعداد ریپورت های ارسال شده:{}✅
تعداد به ازای هر ۱۰ تا افزایش پیدا میکند...

"""
    error_forms = """
    خطا:
    {}
    تعداد:    {}
    \n
    
    """
    block_now_result = """
    در حال انجام عملیات بلاک ...
تعداد بلاک های انجام شده🚫:{}
    """
    block_end_result = """

عملیات بلاک به اتمام رسید✅

زمان صرف شده⏱:{}

تعداد بلاک های موفق🎉:   {}


{}
بلاک های ناموفق❗️:



**{}**
    """
    block_error = """
    
نام کاربری : 
{}
خطا:
{}

    """
    no_reply = "برای انجام این دستور باید پیامی را reply کنید❗️"
    no_forward = "این دستور برای بلاک کردن کسی است که پیام از او فروارد شده است❗️"
    reporting = "در حال انجام عملیات بلاک💡"
    report_end = """
عملیات بلاک با موفقیت انجام شد✅
زمان صرف شده⏱: {}

**{}**
    """
    report_lose = """
    در طی انجام عملیات خطایی رخ داد❗️
خطای رخ داده:
{}
    """
    msg_ids_err = """
خطایی رخ داد❗️
شناسه پیام ها باید به صورت عدد انگلیسی باشند و با فاصله از هم جدا شده باشند
    """
    wrong_format = """
خطایی رخ داد❗️
فرمت ورودی ایراد دارد❌
    """
class Bot:
    api_id = 2383049
    api_hash = "91044b025c7ade0dfb8747b0ee543855"    
    admin = 1

reasons = {"spam":types.InputReportReasonSpam(), "violence":types.InputReportReasonViolence(), "porn":types.InputReportReasonPornography()
          ,"childabuse":types.InputReportReasonChildAbuse(), "geoirrelevant":types.InputReportReasonGeoIrrelevant(),
          "scam": types.InputReportReasonCopyright()}


logging.basicConfig(format='[%(levelname) 5s/%(asctime)s] %(name)s: %(message)s',
                    level=logging.WARNING)


client = TelegramClient("pro", Bot.api_id, Bot.api_hash)
@client.on(events.NewMessage)

async def main(event):
    from telethon import errors
    text = event.raw_text
    if Bot.admin == 0:
        me = await client.get_entity("me")
        Bot.admin = me.id
    
    if(text in [None,""]):
        return
    if(event.from_id == Bot.admin):
        if(text.split()[0] in ["ping","Ping"]):
            time.sleep(random.randint(1,10)/20)
            await client.edit_message(event.chat_id, event.message, texts.ping)
            return
        text = text.split("\n")
        
        if(text[0].lower() in ["block reply", "block this","block this reply", "br", "btr"]):
            await client.edit_message(event.chat_id, event.message, texts.checking)
            time.sleep(random.randint(20,30)/100)
            msg = await event.get_reply_message()
            
            if(msg == None):
                await client.edit_message(event.chat_id, event.message, texts.no_reply)
                return
            
            from_id = msg.from_id
            try:
                user =  await client.get_entity(from_id)
            except Exception as e:
                await client.edit_message(event.chat_id, event.message, texts.entit_error.format(e))
                return
            await client.edit_message(event.chat_id, event.message, texts.reporting)
            time.sleep(random.randint(20,30)/100)
            timee = time.time()
            error = ""
            try:
                result = await client(functions.contacts.BlockRequest(
                                        id=user
                                        ))
                if(result != True):
                    error = "Request Failed"
            except Exception as e:
                error = str(e)
            timee = str(round(time.time() - timee,2)) + "  ثانیه"
            if(error == ""):
                p = random.choice(texts.pray)
                text = texts.report_end.format(timee,p)
                await client.edit_message(event.chat_id, event.message, text)
                
            else:
                text = texts.report_lose.format(error)
                await client.edit_message(event.chat_id, event.message, text)
            return
        if(text[0].lower() in ["block forward", "block this forward", "btf" , "bf"]):
            await client.edit_message(event.chat_id, event.message, texts.checking)
            time.sleep(random.randint(20,30)/100)
            msg = await event.get_reply_message()
            if(msg == None):
                await client.edit_message(event.chat_id, event.message, texts.no_reply)
                return
            try:
                from_id = msg.fwd_from.from_id
               
            except:
                await client.edit_message(event.chat_id, event.message, texts.no_forward)
                return
            try:
                user =  await client.get_entity(from_id)
            except Exception as e:
                await client.edit_message(event.chat_id, event.message, texts.entit_error.format(e))
                return
            await client.edit_message(event.chat_id, event.message, texts.reporting)
            time.sleep(random.randint(20,30)/100)
            timee = time.time()
            error = ""
            try:
                result = await client(functions.contacts.BlockRequest(
                                        id=user
                                        ))
                if(result != True):
                    error = "Request Failed"
            except Exception as e:
                error = str(e)
            timee = str(round(time.time() - timee,2)) + "  ثانیه"
            if(error == ""):
                p = random.choice(texts.pray)
                text = texts.report_end.format(timee,p)
                await client.edit_message(event.chat_id, event.message, text)
                
            else:
                text = texts.report_lose.format(error)
                await client.edit_message(event.chat_id, event.message, text)
            return
            
            
                
            
        if(text[0].lower() == "block"):
            
            success = 0
            n = 1
            block_err = ""
            timee = time.time()
            for username in text[1:]:
                try:
                    result = await client(functions.contacts.BlockRequest(
                                    id=username
                                    ))
                    if(result == True):
                        success += 1
                    else:
                        
                        block_err = texts.block_error.format(username, "Failed Request") + "\n\n"
                except Exception as E:
                    E = str(E)
                    block_err = texts.block_error.format(username, E) + "\n\n"
                await client.edit_message(event.chat_id, event.message, texts.block_now_result.format(n))
                n += 1
                time.sleep(random.randint(30,50)/100)
            timee = str(round(time.time()-timee,2)) + "  ثانیه"
            if(block_err == ""):
                block_err = "بلاک ناموفقی وجود ندارد🎉"
            p = random.choice(texts.pray)
            result = texts.block_end_result.format(timee,success,p,block_err)
            await client.edit_message(event.chat_id, event.message, result)
            return
        if(text[0].lower() == "report"):
            try:
                link_type = text[1]
                link = text[2]
                report_types = text[3].lower().split(" ")
                counts = int(text[4])
                reports = reasons.keys()
            except:
                await client.edit_message(event.chat_id, event.message, texts.wrong_format)
                return
            
            for report_type in report_types:
                if(report_type not in reports):
                    await client.edit_message(event.chat_id, event.message, texts.wrong_type.format(report_type))
                    return
                    
            if(link_type in ["public","Public"]):
                
                await client.edit_message(event.chat_id, event.message, texts.join)
                time.sleep(random.randint(1,10)/10)
                
                try:
                    await client(ImportChatInviteRequest(link))
                except:pass
                await client.edit_message(event.chat_id, event.message, texts.check)
                time.sleep(random.randint(1,10)/10)
                try:
                    channel = await client.get_entity(link)
                except errors.rpcerrorlist.InviteHashExpiredError:
                    
                    
                    t = texts.expected_error.format(texts.invalid_err)
                    await client.edit_message(event.chat_id, event.message, t)
                    return
                except errors.rpcerrorlist.InviteHashInvalidError or errors.rpcerrorlist.InviteHashEmptyError:
                    t = texts.expected_error.format(texts.invalid_link)
                    await client.edit_message(event.chat_id, event.message, t)
                    return
                except Exception as E:
                    t = texts.error.format(str(E))
                    await client.edit_message(event.chat_id, event.message, t)
                    return
                last_link = link
            elif(link_type in ["private","Private"]):
                link = link.split("/")[-1]
                
                
                await client.edit_message(event.chat_id, event.message, texts.join)
                time.sleep(random.randint(1,10)/10)
                
                try:
                    await client(ImportChatInviteRequest(link))
                except:pass
                await client.edit_message(event.chat_id, event.message, texts.check)
                time.sleep(random.randint(1,10)/10)
                try:
                    channel = await client.get_entity(f"https://t.me/joinchat/{link}")
                except errors.rpcerrorlist.InviteHashExpiredError:
                    
                    
                    t = texts.expected_error.format(texts.invalid_err)
                    await client.edit_message(event.chat_id, event.message, t)
                    return
                except errors.rpcerrorlist.InviteHashInvalidError or errors.rpcerrorlist.InviteHashEmptyError:
                    t = texts.expected_error.format(texts.invalid_link)
                    await client.edit_message(event.chat_id, event.message, t)
                    return
                except Exception as E:
                    t = texts.error.format(str(E))
                    await client.edit_message(event.chat_id, event.message, t)
                    return
                last_link = f"https://t.me/joinchat/{link}"
            else:
                t = texts.link_type_err.format(str(link_type))
                await client.edit_message(event.chat_id, event.message, t)
                return
            await client.edit_message(event.chat_id, event.message, texts.start)
            time.sleep(1)
            
            errors = {}
            success = 0
            fail = 0
            n = 0
            timee = time.time()
            for i in range(counts):
                    for report_type in report_types:
                        n += 1
                        try:
                            res = await client(functions.account.ReportPeerRequest(peer=channel, reason=reasons[report_type]))
                            if(res == True):
                                success += 1
                            else:
                                fail += 1
                                
                        except Exception as e:
                            e = str(e)
                            try:
                                errors[e] += 1
                            except:
                                errors.update({e:1})
                        time.sleep(random.randint(20,30)/20)
                        if(n%10 == 0):
                            t = texts.now_result.format(n)
                            await client.edit_message(event.chat_id, event.message, t)
            timee = str(round(time.time() - timee,2)) + " ثانیه" 
            error_str = ""
            if(len(errors.keys()) == 0):
                error_str = "\n" + "خطایی وجود نداشت"
            else:
                for key in errors.keys():
                   error_str += "\n" + (texts.error_forms.format(key,errors[key])) 
                   fail += errors[key]
            report_typess = ""
            try:
                await client(LeaveChannelRequest(channel))
            except:
                try:
                    await client(functions.messages.DeleteChatUserRequest(
                    chat_id=channel.id,
                    user_id='me'
                    ))
                except:pass

            for i in report_types: report_typess+= i + "  " 
            p = random.choice(texts.pray)
            t = texts.result_form.format(timee,last_link,report_typess,success,fail,p,error_str)
            await client.edit_message(event.chat_id, event.message, t)
            return
###
    if(text[0].lower() in ["report messages","report message", "report msgs", "report msg"]):
            try:
                link_type = text[1]
                link = text[2]
                report_types = text[4].lower().split(" ")
                messages_id = text[3].split(" ")
                counts = int(text[5])
                reports = reasons.keys()
            except:
                await client.edit_message(event.chat_id, event.message, texts.wrong_format)
                return
            for i in range(len(messages_id)):
            
                try:messages_id[i] = int(messages_id[i])
                except:
                    await client.edit_message(event.chat_id, event.message, texts.msg_ids_err)
                    return
            
            for report_type in report_types:
                if(report_type not in reports):
                    await client.edit_message(event.chat_id, event.message, texts.wrong_type.format(report_type))
                    return
                    
            if(link_type in ["public","Public"]):
                
                await client.edit_message(event.chat_id, event.message, texts.join)
                time.sleep(random.randint(1,10)/10)
                
                try:
                    await client(ImportChatInviteRequest(link))
                except:pass
                await client.edit_message(event.chat_id, event.message, texts.check)
                time.sleep(random.randint(1,10)/10)
                try:
                    channel = await client.get_entity(link)
                except errors.rpcerrorlist.InviteHashExpiredError:
                    
                    
                    t = texts.expected_error.format(texts.invalid_err)
                    await client.edit_message(event.chat_id, event.message, t)
                    return
                except errors.rpcerrorlist.InviteHashInvalidError or errors.rpcerrorlist.InviteHashEmptyError:
                    t = texts.expected_error.format(texts.invalid_link)
                    await client.edit_message(event.chat_id, event.message, t)
                    return
                except Exception as E:
                    t = texts.error.format(str(E))
                    await client.edit_message(event.chat_id, event.message, t)
                    return
                last_link = link
            elif(link_type in ["private","Private"]):
                link = link.split("/")[-1]
                
                
                await client.edit_message(event.chat_id, event.message, texts.join)
                time.sleep(random.randint(1,10)/10)
                
                try:
                    await client(ImportChatInviteRequest(link))
                except:pass
                await client.edit_message(event.chat_id, event.message, texts.check)
                time.sleep(random.randint(1,10)/10)
                try:
                    channel = await client.get_entity(f"https://t.me/joinchat/{link}")
                except errors.rpcerrorlist.InviteHashExpiredError:
                    
                    
                    t = texts.expected_error.format(texts.invalid_err)
                    await client.edit_message(event.chat_id, event.message, t)
                    return
                except errors.rpcerrorlist.InviteHashInvalidError or errors.rpcerrorlist.InviteHashEmptyError:
                    t = texts.expected_error.format(texts.invalid_link)
                    await client.edit_message(event.chat_id, event.message, t)
                    return
                except Exception as E:
                    t = texts.error.format(str(E))
                    await client.edit_message(event.chat_id, event.message, t)
                    return
                last_link = f"https://t.me/joinchat/{link}"
            else:
                t = texts.link_type_err.format(str(link_type))
                await client.edit_message(event.chat_id, event.message, t)
                return
            await client.edit_message(event.chat_id, event.message, texts.start)
            time.sleep(1)
            
            errors = {}
            success = 0
            fail = 0
            n = 0
            timee = time.time()
            
            for i in range(counts):
                    for report_type in report_types:
                        n += 1
                        try:

                            res = await client(functions.messages.ReportRequest(
                                                peer=channel,
                                                id=messages_id,
                                                reason=reasons[report_type]
                                               ))
                            if(res == True):
                                success += 1
                            else:
                                fail += 1
                                
                        except Exception as e:
                            e = str(e)
                            try:
                                errors[e] += 1
                            except:
                                errors.update({e:1})
                        time.sleep(random.randint(20,30)/20)
                        if(n%10 == 0):
                            t = texts.now_result.format(n)
                            await client.edit_message(event.chat_id, event.message, t)
            timee = str(round(time.time() - timee,2)) + " ثانیه" 
            error_str = ""
            if(len(errors.keys()) == 0):
                error_str = "\n" + "خطایی وجود نداشت"
            else:
                for key in errors.keys():
                   error_str += "\n" + (texts.error_forms.format(key,errors[key])) 
                   fail += errors[key]
            report_typess = ""
            try:
                await client(LeaveChannelRequest(channel))
            except:
                try:
                    await client(functions.messages.DeleteChatUserRequest(
                    chat_id=channel.id,
                    user_id='me'
                    ))
                except:pass

            for i in report_types: report_typess+= i + "  " 
            msgs = ""
            for i in messages_id: msgs+= str(i) + "  "
            p = random.choice(texts.pray) 
            t = texts.msg_result_form.format(timee,last_link,report_typess,msgs,success,fail,p,error_str)
            await client.edit_message(event.chat_id, event.message, t)
                
client.start()
client.run_until_disconnected()         

                

